//mode modules > lit element.js 
import { LitElement, html, css } from 'lit-element';
import 'fa-icons';

class BookSearch extends LitElement{

    static get styles(){
        return css`
            .search{
              padding: 2em;
            }
        `;
      }

    static get properties() {
        return {
            valueSearch: {type: String}
        };
    }

    constructor() {
        super();
    }

    render(){
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <div class="container-fluid search">
                <form class="d-flex">
                    <input class="form-control me-2" placeholder="Titulo, autor" aria-label="Search" id="valueSearchId"> 
                    <button class="btn btn-outline-dark" @click="${this.searchBook}" >BUSCAR</button>
                </form>
            </div>
        `;
    }

    searchBook(e){
        console.log("searchBook");
        e.preventDefault();

        this.valueSearch = this.shadowRoot.getElementById('valueSearchId').value;
        console.log(this.valueSearch);

        this.dispatchEvent(
            new CustomEvent("search-book", {
                    detail: {
                        valueSearch: this.valueSearch
                    }
                }
            )
        );

    }

}

customElements.define("book-search", BookSearch)