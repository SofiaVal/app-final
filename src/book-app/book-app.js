import { LitElement, html } from 'lit-element';
import '../book-header/book-header.js';
import '../book-slider/book-slider.js';
import '../book-footer/book-footer';
import '../book-main/book-main';
class BookApp extends LitElement {

  static get properties() {
    return {
      favsBookArray: {type: Array},
      books: {type: Array}
    };
  }

  constructor() {
      super();
      this.favsBookArray = [];
      this.books = [];
  }


  render() {
      return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <book-header></book-header>
        <div class="container">
          <div class="row">
              <book-main>
              </book-main>
          </div>
        </div>
        <book-footer></book-footer>
      `;
  }

}

customElements.define('book-app', BookApp)
