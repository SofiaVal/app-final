import { LitElement, html, css } from 'lit-element';
import '../book-main-dm/book-main-dm.js';
import '../book-file/book-file.js';
import '../book-detail/book-detail';
import '../book-search/book-search';
import '../book-favs/book-favs.js';
class BookMain extends LitElement {

  static get properties() {
    return {
      books: {type: Array},
      detailBook: {type: Object},
      loading: {type: Boolean},
      showBookDetails: {type: Boolean},
      valueSearch: {type: String},
      favsBookArray: {type: Array}
    };
  }

  constructor() {
    super();
    this.books = [];
    this.loading = false;
    this.detailBook = {};
    this.showBookDetails = false;
    this.favsBookArray = [];
  }

  render() {
    return html `
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
      <book-main-dm @book-file="${this.getBooks}"></book-main-dm>
      <!--<book-slider></book-slider>-->
      <div div class="row" id="idSearch">
        <book-search id="searchBook" @search-book="${this.getSearch}"></book-search>
      </div>
      <div class="row" id="booklist">
        <div class="row row-cols-1 row-cols-sm-4">
          ${
             !this.loading ? html`<fa-icon class="fas fa-spinner" style="margin: 0 auto;padding: 1em;" color="black" size="2em"></fa-icon>` :
             this.books.filter(
                book => book.title.toLowerCase() === this.valueSearch || book.author.toLowerCase() === this.valueSearch || this.valueSearch === undefined || this.valueSearch === ""
             ).map(
                book => html `<book-file @detail-book="${this.getDetailBook}"
                  @favs-book="${this.favsBook}"
                    id="${book.primary_isbn13}"
                    author="${book.author}"
                    photo="${book.book_image}"
                    .buyLinks="${book.buy_links}"
                    description="${book.description}"
                    price="${book.price}"
                    publisher="${book.publisher}"
                    title="${book.title}"
                  >
                </book-file>`
              )
           }
        </div>
      </div>
      <book-detail @book-detail-close="${this.bookDetailClose}" id="detailView" .detailBook=${this.detailBook}></book-detail>
      <book-favs @delete-fav="${this.deleteFavs}"></book-favs>
    `;
  }

  deleteFavs(e) {
     this.favsBookArray = this.favsBookArray.filter(
        book => book.title != e.detail.title
        );
       this.shadowRoot.querySelector("book-favs").favsBookArray = this.favsBookArray;
  }

  getDetailBook(e){ //ocultar y mostrar
    console.log("getDetailBook");
    this.detailBook = e.detail;
    this.showBookDetails = true;

  }

  favsBook(e) {
    console.log("favsBook");
    console.log(e.detail);
    console.log(this.favsBookArray);
    console.log(this.favsBookArray.find(book => book.title === e.detail.title))

    this.favsBookArray.find(book => book.title === e.detail.title) ?
      this.favsBookArray : this.favsBookArray = [...this.favsBookArray, e.detail];

    this.shadowRoot.querySelector("book-favs").favsBookArray = this.favsBookArray;

    console.log(this.favsBookArray);

  }

  getBooks(e) {
    this.books = e.detail.books;
    this.loading = e.detail.loading;

    this.dispatchEvent(new CustomEvent("get-books", {
      detail: {
        books : this.books
      }
    }

    ))
  }

  updated(changedProperties) {
    console.log("updated");

    if(changedProperties.has('showBookDetails')) {
        console.log("ha cambiado el valor del apropierdad showBookDetails en book-main");

        if(this.showBookDetails) {
          this.showDetail();
        }else {
          this.hideDetail();
        }
    }

    if(changedProperties.has('book')){
      console.log('book updated');
      console.log(this.book);
    }

  }

  showDetail() {
    console.log("mostramos el detalle del libro");

    this.shadowRoot.getElementById('booklist').classList.add('d-none');
    this.shadowRoot.getElementById('searchBook').classList.add('d-none');
    this.shadowRoot.getElementById('detailView').classList.remove('d-none');
  }

  hideDetail() {
    console.log("ocultamos el detalle del libro");

    this.shadowRoot.getElementById('booklist').classList.remove('d-none');
    this.shadowRoot.getElementById('searchBook').classList.remove('d-none');
    this.shadowRoot.getElementById('detailView').classList.add('d-none');
  }

  bookDetailClose() {
    this.showBookDetails = false;
  }

  getSearch(e){
    console.log("getSearch")
    this.valueSearch = e.detail.valueSearch.toLowerCase();
    console.log(this.valueSearch);

  }

}
customElements.define('book-main', BookMain);
