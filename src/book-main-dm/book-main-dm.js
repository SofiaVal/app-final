import {LitElement, html} from 'lit-element';

class BookMainDM extends LitElement{

    static get properties(){
        return {
            books:{type: Array},
            loading:{type: Boolean}
        };
    }
    constructor (){
        super ();

        this.books = [];
        this.getBookData();
        this.updated();
    }

    getBookData(){
        console.log("getBookData");
        console.log("Obteniendo datos de los libros");

        let xhr = new XMLHttpRequest();
        var url = "https://api.nytimes.com/svc/books/v3/lists/current/hardcover-fiction.json?api-key=YMS759yG5LubAS43MSQWTMZDPmOIkbFo";

        xhr.onload = () => {
            if (xhr.status === 200){
                console.log("Peticion completada correctamente");

                let APIResponse = JSON.parse(xhr.responseText);
                this.books = APIResponse.results.books;
                this.loading = true;
                
            }
        }
        xhr.open("GET", url);
        xhr.send();
    }
    
    updated(changedProperties){
            console.log("updated book-main-DM");

           this.dispatchEvent(new CustomEvent("book-file",{
                detail:{
                    books: this.books,
                    loading: this.loading
               }
            }))
       

    }

}

customElements.define('book-main-dm', BookMainDM);
