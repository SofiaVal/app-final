import { LitElement, html, css } from 'lit-element';

class BookHeader extends LitElement {

  
  static get styles(){
    return css`
        .black{
            background-color: #212529!important;
            color: white;
            text-align: center;
        }
        h2{
          padding: 0.2em;
        }
    `;
}

  static get properties() {
    return {
        
    };
  }

  constructor() {
      super();

  }


  render() {
      return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
      <div class="black">
        <h2>Listado de libros</h2>
      </div>
      `;
  }

}

customElements.define('book-header', BookHeader)
