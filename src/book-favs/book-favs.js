import { LitElement, html, css } from 'lit-element';
import 'fa-icons';

class BookFavs extends LitElement {

  static get styles(){
    return css`
        h2{
          text-align: center;
          margin: 0 auto;
          color: darkgrey;
        }
    `;
  }

  static get properties() {
    return {
      favsBookArray: {type: Array}

    };
  }

  constructor() {
      super();
      this.favsBookArray = [];

  }


  render() {
      return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
      <div class="row" id="booklist">
        <div>
          <hr class="line" style="margin-top: 5rem;">
          <h4 class="text-center">Selección de libros favoritos</h4>
        </div>
        <div class="row row-cols-1 row-cols-sm-4">
            ${
            !this.favsBookArray.length>0 ?
             html`<h2>Añade tu libro favorito</h2>`: this.favsBookArray.map(
                favsBook => html`
                <div class="container">
                    <div class="card h-100" style="width: 18rem;">
                        <img class="card-img-top" src="${favsBook.photo}" alt="${favsBook.title}"/>
                        <div class="card-body">
                            <h5 class="card-title">${favsBook.title}</h5>
                            <p class="card-text">${favsBook.author}</p>
                            <button id="${favsBook.title}" @click="${this.delete}" class="btn col-5 me-md-2"> <fa-icon class="fas fa-trash-alt" color="${this.colorStart}" size="2em"></fa-icon></button>
                          </div>
                      </div>
                  </div>
              `)
            }
          </div>
      </div>
      `;
  }

  delete(e) {
    console.log("vamos a borrar el favorito " + e.currentTarget.id);
   this.dispatchEvent(new CustomEvent("delete-fav", {
     detail: {
       title: e.currentTarget.id
     }
   }))
  }

}

customElements.define('book-favs', BookFavs)
