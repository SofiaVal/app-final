import {LitElement, html} from 'lit-element';

class BookDetail extends LitElement{

    static get properties(){
        return{
          detailBook: {type: Object},
        };
    }

    constructor(){

        super();
    }

 
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">     
            <div class="card mb-3" style="max-width: 1200px; margin: 0 auto;">
            <div class="row g-0" >
              <div class="col-md-4">
                <img src="${this.detailBook.photo}" alt="${this.detailBook.title}">
                <br><br>
                <ul class="nav " >
                  <a class = "offset-md-1" href="https://www.facebook.com/LibrosInfinite" target="_blank">
                    <fa-icon  class="fab fa-facebook">
                    </fa-icon >
                  </a>
                  <a class = "offset-md-1" href="https://twitter.com/librosylit" target="_blank">
                    <fa-icon  class="fab fa-instagram-square">
                    </fa-icon >
                  </a>
                  <a class = "offset-md-1" href="https://www.gmail.com/" target="_blank">
                    <fa-icon  class="fas fa-envelope-square">
                    </fa-icon >
                   </a>
                </ul>
                <br><br>
              </div>
              <div class="col-md-8">
                <div class="card-body">
                  <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                    <button @click="${this.goBack}" class="offset-md-4 btn btn-outline-dark col-4"><strong>ATRÁS</strong></button>         
                  </div> 
                  <h5 class="card-title">${this.detailBook.title}</h5>
                  <h6 class="card-subtitle mb-2 text-muted">${this.detailBook.author}</h6>
                  <p class="card-text">${this.detailBook.description}</p>
                  <p class="card-text"><small class="text-muted">Publisher: ${this.detailBook.publisher}</small></p>
                  <p class="card-text">Enlaces de compra: </p>
                    <ul  class="list-group list-group-flush">${
                      this.detailBook.buyLinks === undefined ? html `` : 
                      this.detailBook.buyLinks.map(
                          buyLink => html `
                          <ul class="nav" >
                          <fa-icon class="fas fa-shopping-cart"> 
                            </fa-icon>
                            <a class="offset-md-1" href="${buyLink.url}" target="_blank">${buyLink.name}</a>
                          </ul>
                          `
                      )}
                    </ul>
                </div>
                </div>
              </div>
             </div>       
        `;
    }

    updated(changedProperties){

      console.log("updated book detail");

      if(changedProperties.has('detailBook')) {
          console.log("Book detail"+this.detailBook.buyLinks)
      }

    }

    goBack(e){
      console.log("goBack");
      e.preventDefault();
    
      this.dispatchEvent(new CustomEvent("book-detail-close",{}));

    
    }

}

customElements.define('book-detail', BookDetail);
