import { LitElement, html, css } from 'lit-element';

class BookSlider extends LitElement {

  static get properties() {
    return css`
        .black{
            background-color: #212529!important;
            color: white;
            text-align: center;
        }
        h2{
          padding: 0.2em;
        }
    `;
  }

  constructor() {
      super();
      

  }


  render() {
      return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
      <div class="container">
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-indicators">
             <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
          </div>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="https://image.freepik.com/foto-gratis/libro-biblioteca-libro-texto-abierto_1150-5920.jpg" 
              class="d-block w-100" alt="..."
              height="200">
            </div>
            <div class="carousel-item">
              <img src="https://image.freepik.com/foto-gratis/libro-biblioteca-libro-texto-abierto_1150-5918.jpg" 
              class="d-block w-100" alt="..."
              height="200">
            </div>
            <div class="carousel-item">
              <img src="https://image.freepik.com/foto-gratis/vista-superior-libros-abiertos-sobre-mesa_23-2148448109.jpg" 
              class="d-block w-100" alt="..."
              height="200">
            </div>
          </div>
          <button class="carousel-control-prev" type="button" 
            data-bs-target="#carouselExampleInterval"
            data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" 
          data-bs-target="#carouselExampleInterval"
          data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
      </div>
      
      `;
  }

}

customElements.define('book-slider', BookSlider)
