//mode modules > lit element.js
import { LitElement, html } from 'lit-element';
import 'fa-icons';

class BookFile extends LitElement{

    static get properties() {
        return {
            id:{type:String},
            title:{type:String},
            author:{type:String},
            description:{type:String},
            photo: {type: String},
            publisher: {type: String},
            buyLinks:{type:Array},
            price:{type:String},
            colorStart: {type: String}

        };
    }

    constructor() {
        super();
        //this.colorStart="#E3E2E2";
        this.colorStart="black";
    }

    render(){
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
                <div class="card h-100" style="width: 18rem;">
                    <img class="card-img-top" src="${this.photo}" alt="${this.title}">
                    <div class="card-body">
                        <h5 class="card-title">${this.title}</h5>
                        <p class="card-text">${this.author}</p>
                    </div>
                    <div class="card-footer">
                        <button @click="${this.moreDetail}" class="btn btn-outline-dark col-5"><strong>VER MÁS</strong></button>
                        <button @click="${this.favs}" id="${this.title}" class="btn col-5"> <fa-icon class="fas fa-star" color="${this.colorStart}" size="2em"></fa-icon></button>
                    </div>
                </div>
            </div>
        `;
    }

    favs(e){
        console.log("favs= "+this.author);
        //this.colorStart="black";

        console.log(this.photo);

       this.dispatchEvent(
            new CustomEvent("favs-book", {
                    detail: {
                        id:this.id,
                        title:this.title,
                        author:this.author,
                        description: this.description,
                        photo: this.photo
                    }
                }
            )
        );

    }

    moreDetail(e){
        console.log("moreDetail");
        console.log("Se ha pedido mas informacion del libro "+this.author+ ", id: "+this.id);

        this.dispatchEvent(
            new CustomEvent("detail-book", {
                detail: {
                    id:this.id,
                    title:this.title,
                    author:this.author,
                    description: this.description,
                    photo: this.photo,
                    buyLinks:this.buyLinks,
                    price:this.price,
                    publisher: this.publisher
                }
            }
        ));
    }


}

customElements.define("book-file", BookFile)
